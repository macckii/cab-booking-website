<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Custom validation functions.
*/

require_once("db/request.php");

// Checks that given date and time is not before the buffer
function checkTimeBuffer($date, $time, $buffer = "+59 minutes") {
    $requestedTime = date_create_from_format("Y-m-d H:i", $date . " " . $time);
    $validTime = new DateTime();
    $validTime->setTimestamp(strtotime($buffer));
    
    return $requestedTime >= $validTime;
}

function isUnique($data, $type, $tableName) {
    $query = "SELECT * FROM {$tableName} WHERE {$type} = '{$data}' LIMIT 1";
    
    $result = dbRead($query);
      
    return count($result) == 0;
}

?>