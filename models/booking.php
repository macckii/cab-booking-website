<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Booking model class which respresents a booking record from DB.
*/

require_once("active_record/model.php");
require_once("validation/custom_validations.php");
require_once("models/customer.php");

class Booking extends Model {

    protected $fields = array(
        "booking_id",
        "passenger_name",
        "passenger_phone",
        "unit_number",
        "street_number",
        "street_name",
        "suburb",
        "destination_suburb",
        "pickup_date",
        "pickup_time",
        "email",
        "status",
        "booking_datetime",
    );

    function __construct($data = array()) {
        $this->validator = new Validator($this);
        $this->data = $data;
        $this->data['status'] = "unassigned";
    }
    
    function beforeCreate() {
        $this->data['booking_id'] = uniqid();
        $date = new DateTime(null, new DateTimeZone("Australia/Melbourne"));
        $this->data['booking_datetime'] = $date->format("Y-m-d H:i");
    }
    
    function getValidations() {
        return array(
            "passenger_name" => array(
                "required",
                array('type' => "max", 'value' => 30),
            ),
            'passenger_phone' => array(
                "required",
                array('type' => "equal", 'value' => 10)
            ),
            'unit_number' => array(
                array('type' => "max", 'value' => 5)
            ),
            'street_number' => array(
                "required",
                array('type' => "max", 'value' => 5)
            ),
            'street_name' => array(
                "required",
                array('type' => "max", 'value' => 60)
            ),
            'suburb' => array(
                "required",
                array('type' => "max", 'value' => 30)
            ),
            'destination_suburb' => array(
                "required",
                array('type' => "max", 'value' => 30)
            ),
            'pickup_date' => array(
                "required",
                array('type' => "time_format", 'value' => "Y-m-d")
            ),
            'pickup_time' => array(
                "required",
                array('type' => "time_format", 'value' => "H:i")
            ),
            "pickup_datetime" => array(
                array(
                    'type'       => "custom",
                    'fields'     => array("pickup_date", "pickup_time"),
                    'condition'  => function($v) { return strlen($v[0]) > 0 && strlen($v[1]) > 0 ? checkTimeBuffer($v[0], $v[1]) : true; },
                    'error_msg'  => "Pickup time must be at least 1 hour later than the current time",
                )
            ),
        );
    }
    
    function getAddressString() {
        $d = $this->data;
        $unitNumber = strlen($d['unit_number']) == 0 ? "" : $d['unit_number'] . " / ";
        return $unitNumber . $d['street_number'] . " " . $d['street_name'] . ", " . $d['suburb'];
    }
    
    function getCustomer() {
        $criteria = array("email = '" . $this->data['email'] . "'");
        $response = Customer::find($criteria);
        if (count($response) > 0) return $response[0];
    }
    
    function sendConfirmationEmail() {
        $to = $this->data['email'];
        $subject = "Your booking request with CabsOnline!";
        $customer = $this->getCustomer()->getSerialized();
        $message = "Dear {$customer['name']}, Thanks for booking with CabsOnline! Your booking reference number is {$this->data['booking_id']}. We will pick up the passengers in front of your provided address at {$this->data['pickup_time']} on {$this->data['pickup_date']}.";
        $header = "From booking@cabsonline.com.au";
        $parameters = "-r 100059374@student.swin.edu.au";
        return mail($to, $subject, $message, $header, $parameters);
    }

}


?>