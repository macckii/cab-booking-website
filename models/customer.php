<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Customer model class which respresents a customer record from DB.
*/

require_once("active_record/model.php");

class Customer extends Model {
    
    protected $fields = array(
        "name",
        "password",
        "email",
        "phone",
    );

    function getValidations() {
        return array(
            "name" => array(
                "required",
                array('type' => "max", 'value' => 30),
            ),
            'password' => array(
                "required",
                array('type' => "max", 'value' => 15)
            ),
            'email' => array(
                "required",
                "unique",
                array('type' => "max", 'value' => 60)
            ),
            'phone' => array(
                "required",
                array('type' => "equal", 'value' => 10)
            ),
        );
    }

    function beforeCreate() {
        $this->data['email'] = strtolower(trim($this->data['email']));
    }

}


?>