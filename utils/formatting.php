<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for string formatting.
*/

function humanize($str) {
    return ucfirst(str_replace("_", " ", $str));
}

?>