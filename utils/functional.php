<?php
/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for data manipulation.
*/

function unpackData($input, $fields, $default = "") {
    $unpacked = null;
    
    foreach($fields as $field) {
        $unpacked[$field] = isset($input[$field]) ? $input[$field] : $default;  
    }
    
    return $unpacked;
}

function unpackFlatten($input, $fields) {
    $result = array();
    foreach ($fields as $f) {
        if (isset($input[$f])) array_push($result, $input[$f]);
    }
    
    return $result;
}

function refValues($arr){
    $refs = array();
    foreach($arr as $key => $value)
        $refs[$key] = &$arr[$key];
    return $refs;
}

function permittedAttributes($obj, $attrs) {
    $result = array();
    foreach ($attrs as $attr) {
        if (isset($obj[$attr])) $result[$attr] = $obj[$attr];
    }
    
    return $result;
}

?>