<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for logging.
*/

function logError($msg) {
    echo "\n";
    print_r($msg);
    echo "\n\n";
};
function logInfo($msg) {
    echo "\n";
    print_r($msg);
    echo "\n\n";
};
function logDebug($msg) {
    echo "\n";
    print_r($msg);
    echo "\n\n";
};

?>