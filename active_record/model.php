<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Model class (abstract) which represents common traits among model classes.
*/

require_once("active_record/validatable.php");
require_once("active_record/validator.php");
require_once("db/query_builders.php");
require_once("db/request.php");

abstract class Model implements Validatable {

    protected $errors;
    protected $data;
    protected $validator;
    protected $fields;
    protected $primaryKey;
    
    protected static $tableName;
    
    function __construct($data = array()) {
        $this->validator = new Validator($this);
        $this->data = $data;
    }
    
    function setData($data) {
        $this->data = $data;
    }
    
    function get($field) {
        return $this->data[$field];
    }
    
    function set($field, $value) {
        $this->data[$field] = $value;
    }
    
    function getSerialized() {
        return unpackData($this->data, $this->fields);
    }
    
    function getModelName() {
        return strtolower(get_class($this));
    }
    
    function getErrors() {
        return $this->errors;
    }

    function validate() {
        $this->errors = $this->validator->validate();
        return count($this->errors) == 0;
    }
    
    function beforeCreate() {}
    
    function beforeSave() {}
    
    function create() {
        $this->beforeCreate();
        $this->beforeSave();
        
        $serialized = $this->getSerialized();
        $query = buildInsertQuery($this->getTableName(), $serialized);
        
        $params = $this->getBindingParams();
        
        $result = dbWrite($query, $params);

        return $result == 1;
    }
    
    function update() {
        $this->beforeSave();
        
        $serialized = $this->getSerialized();
        $primaryKey = $this->primaryKey ? $this->primaryKey : strtolower(get_called_class()) . "_id";
        $query = buildUpdateQuery($this->getTableName(), $serialized, $primaryKey . " = '" . $serialized[$primaryKey] . "'");
        
        $params = $this->getBindingParams();
        
        $result = dbWrite($query, $params);

        return $result == 1;
    }
    
    private function getBindingParams() {
        $serialized = $this->getSerialized();
        $types = "";
        $params = array();
        foreach($serialized as $k => $v) {
            $types .= "s";
            $params[] = $v;
        }
        
        array_unshift($params, $types);
        
        return $params;
    }
    
    static function getTableName() {
        return self::$tableName ? self::$tableName : get_called_class();
    }
    
    static function find($criteria) {
        $query = buildSelectQuery(self::getTableName(), null, $criteria);
        $response = dbRead($query);
        $result = array();
        foreach ($response as $record) {
            $klass = get_called_class();
            $result[] = new $klass($record);
        }
        
        return $result;
    }
}

?>