<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Validatable interface which defines functions for classes to be validatable.
*/

interface Validatable {
    
    function validate();
    function getValidations();
    function getSerialized();
    
    static function getTableName();
}

?>