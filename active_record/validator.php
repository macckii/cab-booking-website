<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Validator class which provides validating functions.
*/

require_once("utils/functional.php");
require_once("utils/formatting.php");
require_once("utils/logging.php");
require_once("validation/custom_validations.php");


class Validator {

    private $model;
    private $normalizedValidations;
        
    function __construct($model) {
        $this->model = $model;
        $config = $this->model->getValidations();
        foreach ($config as $name => $validations) {
            $this->normalizedValidations[$name] = $this->normalizeValidation($name, $validations);
        }
    }

    function validate() {
        $errors = array();
        $input = $this->model->getSerialized();
        
        foreach ($this->normalizedValidations as $name => $normalizedValidations) {
            foreach ($normalizedValidations as $n) {

                // Each field should only have one validation
                if (isset($errors[$name])) break;
                
                $inputValue = unpackFlatten($input, $n['fields']);
                $inputCount = count($inputValue);
                
                // If no value has been input for this field, there's no need to run the validation
                if ($inputCount == 0) continue;
                if ($inputCount == 1) $inputValue = $inputValue[0];
                
                $cond = $n['condition'];
                
                if (!is_null($cond) && !$cond($inputValue, $name)) $errors[$name] = $n['error_msg'];
            }
        }
        
        return $errors;
    }

    private function normalizeValidation($name, $validations) {
        $result = array();
        
        foreach ($validations as $v) {
            if (is_string($v)) $v = array('type' => $v);           
            if (!isset($v['type'])) {
                logError("Validation missing type");
                continue;
            }
            
            $type      = $v['type'];
            $value     = isset($v['value'])     ? $v['value']     : null;
            $fields    = isset($v['fields'])    ? $v['fields']    : array($name);
            $condition = isset($v['condition']) ? $v['condition'] : $this->getCondition($type, $value);
            $errorMsg  = isset($v['error_msg']) ? $v['error_msg'] : $this->getErrorMsg($type, $value, $name);
            
            $result[] = array(
                'type'       => $type,
                'fields'     => $fields,
                'condition'  => $condition,
                'error_msg'  => $errorMsg,
                'value'      => $value
            );
        }
        
        return $result;
    }

    private function getCondition($type, $value) {
        if ($type == "custom") logError("Validation of type custom configured without a condition");
        
        $tableName = $this->model->getTableName();
        
        $conditions = array(
            'required'    => function($v) { return strlen($v) > 0; },
            'unique'      => function($v, $name) use($tableName) { return isUnique($v, $name, strtoupper($tableName)); },
            'max'         => function($v) use($value) { return strlen($v) <= $value; },
            'equal'       => function($v) use($value) { return strlen($v) == $value; },
            'time_format' => function($v) use($value) {
                $d = DateTime::createFromFormat($value, $v);
                return $d && $d->format($value) === $v;
            },
        );
        
        if (isset($conditions[$type])) return $conditions[$type];
        logError("Validation type {$type} was not found");
    }

    private function getErrorMsg($type, $value, $fieldName) {
        if ($type == "custom") logError("Validation of type custom configured without an error message");
        
         $humanReadableFormats = array(
            'Y-m-d' => "YYYY-MM-DD",
            'H:i' => "HH:MM",
        );
        
        $readable = isset($humanReadableFormats[$value]) ? $humanReadableFormats[$value] : "";
        
        $errorMessages = array(
            'required'    => " is required",
            'unique'      => " must be unique",
            'max'         => " must be less than {$value} characters/digits",
            'equal'       => " must be {$value} characters/digits",
            'time_format' => " must be typed as '{$readable}'",
        );

        if (isset($errorMessages[$type])) return humanize($fieldName) . $errorMessages[$type];
        logError("Error message for type {$type} was not found");
    }
}
?>