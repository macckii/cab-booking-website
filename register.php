<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Register page for CabsOnline.
*/

require_once("models/customer.php");
require_once("rendering/form_helpers.php");
require_once("utils/functional.php");

$customer = new Customer();
$msg;

if (isset($_POST['customer'])) {
    $customerInput = $_POST['customer'];
    if (isset($customerInput['confirm_password']) && isset($customerInput['password'])) {
        
        $customerParams = permittedAttributes($customerInput, array("email", "phone", "password", "name"));        
        $customer = new Customer($customerParams);
            
        if ($customer->validate()) {
            if ($customerInput['confirm_password'] === $customerInput['password']) {
                if ($customer->create()) {
                    session_start();
                    $_SESSION['email'] = $customer->get("email");
                    header("Location: booking.php");
                } else {
                    $msg = "Unable to register at this time. Please try again later.";
                } 
            } else {
                $msg = "Password must match confirm password.";
            }        
        } else {
            $msg = "Please correct the errors below.";
        }
    } else {
        $msg = "Password and confirm password are required";
    }
    
    
} else {
    $msg = "Please fill the fields below to complete your registration";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Registration page for CabsOnline" />
    <meta name="keywords" content="cabs, booking, register, registration" />
    <meta name="author" content="Maki Sugita" />
    <title>Register to CabsOnline</title>
</head>
<body>
    <h1>Register to CabsOnline</h1>
    <?php echo $msg; ?>
    <?php echo modelForm($customer, "POST", "register.php", function($f) {

        $f->textInput("name");
        $f->passwordInput("password");
        $f->passwordInput("confirm_password");
        $f->textInput("email");
        $f->textInput("phone");
        $f->submitButton("Register");
        
    }); ?>

    <p><b>Already registered? <a href="login.php">Login here</a></b></p>
</body>
</html>