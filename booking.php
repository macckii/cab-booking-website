<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Booking page for CabsOnline.
*/

require_once("models/booking.php");
require_once("rendering/form_helpers.php");

// Session control
session_start();
$email = "";
if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
} else {
    header("Location: login.php");
}

$booking;
$msg;

if (isset($_POST['booking'])) {
    $booking = new Booking($_POST['booking']);
    $booking->set("email", $email);
    if ($booking->validate()) {
        if ($booking->create()) {
            $msg = "Thank you! Your booking reference number is {$booking->get('booking_id')}. We will pick up the passengers in front of your provided address at {$booking->get('pickup_time')} on {$booking->get('pickup_date')}.";
            $result = $booking->sendConfirmationEmail();
            $msg .= $result ? "<br />Booking information was sent to your email address." : "<br />Failed to send email";
        } else {
            $msg = "Unable to book at this time. Please try again later.";
        }
  
    } else {
        $msg = "Please correct the errors below.";
    }
} else {
    $booking = new Booking();
    $msg = "Please fill in the fields below to book a taxi.";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Booking page for CabsOnline" />
    <meta name="keywords" content="cabs, booking" />
    <meta name="author" content="Maki Sugita" />
    <title>Booking a cab with CabsOnline</title>
</head>
<body>
    <h1>Booking a cab</h1>
    <p><?php echo $msg; ?></p>
    
    <?php echo modelForm($booking, "POST", "booking.php", function($f) {

        $f->textInput("passenger_name");
        $f->textInput("passenger_phone", "Contact phone of the passenger");
        $f->html("<fieldset><legend>Address</legend>");
        $f->textInput("unit_number");
        $f->textInput("street_number");
        $f->textInput("street_name");
        $f->textInput("suburb");
        $f->html("</fieldset>");
        $f->textInput("destination_suburb");
        $f->getSpecificError("pickup_datetime");
        $f->textInput("pickup_date");
        $f->textInput("pickup_time");
        $f->submitButton("Book");
        
    }); ?>
    
</body>
</html>