<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Helper functions for making html table.
*/

require_once("rendering/table_maker.php");

function modelTable($resultset, $func) {
    
    $tableMaker = new TableMaker($resultset);
    
    $func($tableMaker);
    
    return $tableMaker->render();
}

?>