<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for making html table.
*/

require_once("utils/formatting.php");

class TableMaker {
    
    private $head;
    private $openingTag;
    private $closingTag;
    private $data;
    private $fields;

    function __construct($data) {
        $this->openingTag = "<table border=\"1\">";
        $this->closingTag = "</table>";
        $this->head = "";
        $this->data = $data;
        $this->fields = array();
    }
    
    function render() {
        $tableHead = "<thead><tr>" . $this->head . "</tr></thead>";
        $tableBody = "<tbody>" . $this->rows() . "</tbody>";
        
        return $this->openingTag . $tableHead . $tableBody . $this->closingTag;
    }
        
    function column($attr, $label = null) {
        $label = is_null($label) ? humanize($attr) : $label;
        $this->head .= "<th>" . $label . "</th>";
        $this->fields[] = $attr;
    }
    
    private function rows() {
        $body = "";
 
        foreach ($this->data as $d) {
            $obj = $d->getSerialized();
            $body .= "<tr>";
            
            foreach ($this->fields as $f) {
                $val = is_callable($f) ? $f($d) : $obj[$f];
                $body .= "<td>" . $val . "</td>";
            }
            $body .= "</tr>";
        }
        return $body;
    }
    
}

?>