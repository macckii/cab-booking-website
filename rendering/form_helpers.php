<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Helper functions for making html form.
*/

require_once("utils/formatting.php");
require_once("rendering/form_maker.php");

function textInput($attribute, $value, $error, $label = null, $name = null, $type = "text") {
    $label = is_null($label) ? humanize($attribute) : $label;
    $label = strlen($error) == 0 ? $label : "<b><i>" . $error . "</i></b>";
    $name = strlen($name) == 0 ? $attribute : $name;
    return "<p><label>{$label}: <br /><input type=\"{$type}\" name=\"{$name}\" value=\"{$value}\" /></label></p>";
}

function modelForm($model, $method, $action, $func) {
    
    $formMaker = new FormMaker($model->getSerialized(), $model->getErrors(), $model->getModelName(), $method, $action);
    
    $func($formMaker);

    return $formMaker->render();
};

?>