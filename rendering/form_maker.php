<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for making html form.
*/

require_once("rendering/form_helpers.php");

class FormMaker {
    
    private $body;
    private $openTag;
    private $closingTag;
    private $modelName;
    private $data;
    private $errors;

    function __construct($data, $errors, $modelName, $method, $action) {
        $this->openingTag = "<form method=\"{$method}\" action=\"{$action}\">";
        $this->closingTag = "</form>";
        
        $this->body = "";
        $this->modelName = $modelName;
        $this->data = $data;
        $this->errors = $errors;
    }
    
    function render() {
        return $this->openingTag . $this->body . $this->closingTag;
    }
    
    function textInput($attr, $label = null, $type = "text") {
        $errors = $this->errors;
        $data = $this->data;
        $modelName = $this->modelName;
        
        $d = isset($data[$attr]) ? $data[$attr] : "";
        $err = isset($errors[$attr]) ? $errors[$attr] : "";
        $modelName = "{$modelName}[{$attr}]";
        $this->body .= textInput($attr, $d, $err, $label, $modelName, $type);
    }
    
    function passwordInput($attr, $label = null) {
        $this->textInput($attr, $label, "password");
    }
    
    function html($html) {
        $this->body .= $html;
    }
    
    function getSpecificError($attr) {
        $error = isset($this->errors[$attr]) ? "<b><i>" . $this->errors[$attr] . "</i></b><br />" : "";
        $this->body .= $error;
    }
    
    function submitButton($value) {
        $this->body .= "<input type=\"submit\" value=\"{$value}\" />";
    }
    
}

?>