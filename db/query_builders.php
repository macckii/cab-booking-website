<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for building queries.
*/

function buildSelectQuery($tablename, $fields, $criteria) {
    $delimiter = ", ";
    $fields_str = is_null($fields) ? "*" : join($delimiter, $fields);
    $delimiter = " AND ";
    $criteria_str = is_null($criteria) ? "" : " WHERE " . join($delimiter, $criteria);
    $query = "SELECT " . $fields_str . " FROM " . strtoupper($tablename).$criteria_str;
    
    return $query;
}

function buildInsertQuery($tablename, $dataSet) {
    $fields = array();
    $values = array();
    
    foreach ($dataSet as $field => $data) {
        array_push($fields, $field);
        array_push($values, "?");
    }

    $delimiter = ", ";
    $fields_str = join($delimiter, $fields);
    $values_str = join($delimiter, $values);
    $query = "INSERT INTO " . strtoupper($tablename) . " (" . $fields_str . ") VALUES (" . $values_str . ")";
    
    return $query;
}

function buildUpdateQuery($tablename, $dataSet, $criteria) {
    $assignments = array();
    
    foreach ($dataSet as $field => $data) {
        array_push($assignments, $field . " = ?");
    }

    $query = "UPDATE " . strtoupper($tablename) . " SET " . join(", ", $assignments) . " WHERE " . $criteria;
    
    return $query;
}

?>