<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Functions for database operations.
*/

require_once("config.php");
require_once("utils/functional.php");

function dbRead($sql) {
    if (isset($GLOBALS['environment'])) {
        if ($GLOBALS['environment'] == "test") {
            $GLOBALS['lastQuery'] = array($sql);
            return array();
        } 
    }
    
    $result = array();
    dbRequest(function($conn) use($sql, &$result) {
        $stmt = $conn->query($sql);
        
        if ($stmt) {
            while ($row = $stmt->fetch_assoc()) {
                $result[] = $row;
            }
            $stmt->free();
        }
    });
    
    return $result;
}

function dbWrite($sql, $params) {

    if (isset($GLOBALS['environment'])) {
        if ($GLOBALS['environment'] == "test") {
            $GLOBALS['lastQuery'] = array($sql, $params);
            return;
        }
    }
    $result = 0;
    
    dbRequest(function($conn) use($sql, $params, &$result) {
        $stmt = $conn->prepare($sql);
        
        if ($stmt === false) {    
            logError("Malformed SQL: " . $sql . " Error: " . $conn->error);
        }
        
        call_user_func_array(array($stmt, "bind_param"), refValues($params));
        
        $stmt->execute();
        $result = $stmt->affected_rows;
        
        $stmt->close();
    });
    
    return $result;
}

function dbRequest($delegate = null) {    
    $host = HOST;
    $user = DB_USER;
    $pw   = DB_PASSWORD;
    $db   = DATABASE;
    
    
    // Get DB connection
    $conn = new mysqli($host, $user, $pw, $db);
    if ($conn->connect_error) {
        printf("Connect failed: %s\n", $conn->connect_error);
        exit();
    }
    
    // Perform DB operation
    $delegate($conn);
    
    // Close connection
    $conn->close();
    
}


?>