<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Login page for CabsOnline.
*/

require_once("models/customer.php");

$err_msg = "";

if (isset($_POST['email']) && isset($_POST['password'])) {

    $email = strtolower(trim($_POST['email']));
    $password = $_POST['password'];
    
    $criteria = array("email = '{$email}'", "password = '{$password}'"); 
    $customer = Customer::find($criteria);
    
    if (count($customer) == 1) {
        session_start();
        $_SESSION['email'] = $email;
        header("Location: booking.php");
        exit();
    } else {
        $err_msg .= "Incorrect email or password";
    }
    
} else {
    $err_msg .= "Email and password are required";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Login page for CabsOnline" />
    <meta name="keywords" content="cabs, booking, Login" />
    <meta name="author" content="Maki Sugita" />
    <title>Login to CabsOnline</title>
</head>
<body>
    <h1>Login to CabsOnline</h1>
    <?php echo $err_msg; ?>
    <form method="POST">
        <p><label>Email: <input type="text" name="email" /></label></p>
        <p><label>Password: <input type="password" name="password" /></label></p>
        <input type="submit" value="Log in" />
    </form>
    <p><b>New member? <a href="register.php">Register now</a></b></p>
</body>
</html>