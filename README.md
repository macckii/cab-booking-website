Cab Booking Website
===================

Why did I make this?
--------------------
This is a sample website adapted from a project I made while completing my Masters Degree in Information Technology. This project demonstrates my ability to make a fully functional web application *without* a framework. While I would not expect to build an architecture like this for a real-life application, it was highly instructive to make this from scratch without external libraries for object-relational mapping, validation, etc.

If you would like to learn more about me, checkout [my LinkedIn profile](https://www.linkedin.com/profile/preview?vpa=pub&locale=en_US).