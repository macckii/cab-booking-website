<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Admin page for CabsOnline.
*/

require_once("models/booking.php");
require_once("rendering/form_helpers.php");
require_once("rendering/table_helpers.php");

$msg = ""; 
$unassigned = ""; 

$currentTime = new DateTime(null, new DateTimeZone("Australia/Melbourne"));
$twoHoursLater = $currentTime->setTimestamp(strtotime("+2 hours"));

// List form
if (isset($_POST['unassigned-list'])) {
    // get booking records within two hours
    $criteria = array(
        "status = 'unassigned'",
        "TIMESTAMP(pickup_date, pickup_time) BETWEEN NOW() AND '" . $twoHoursLater->format("Y-m-d H:i") . "'"
    );
    
    $bookings = Booking::find($criteria);
    
    // make html table
    $unassigned = modelTable($bookings, function($f) {
    
        $f->column("booking_id", "reference #");
        $f->column(function($b) { $c = $b->getCustomer(); return $c->get("name"); }, "customer name");
        $f->column("passenger_name");
        $f->column("passenger_phone", "passenger contact phone");
        $f->column(function($b) { return $b->getAddressString(); }, "pick-up address");
        $f->column("destination_suburb");
        $f->column("pickup_time", "pick-up time");
        
    });

}

// Assignment form
if (isset($_POST['reference_number'])) {
    
    $criteria = array(
        "booking_id = '" . $_POST['reference_number'] . "'",
        "status = 'unassigned'",
        "TIMESTAMP(pickup_date, pickup_time) BETWEEN NOW() AND '" . $twoHoursLater->format("Y-m-d H:i") . "'"
    );
    $result = Booking::find($criteria);
    
    if (count($result) != 0) {
        $booking = $result[0];

        // update the status
        $booking->set("status", "assigned");        
        if ($booking->update()) {
            $msg = "The booking request " . $_POST['reference_number'] . " has been properly assigned.";
        } else {
            $msg = "Failed to update status at this time. Please try again later.";
        }
            
    } else {
        $msg = "Unable to find the unassigned booking matches the reference number.";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Admin page for CabsOnline" />
    <meta name="keywords" content="cabs, admin" />
    <meta name="author" content="Maki Sugita" />
    <title>Admin page of CabsOnline</title>
</head>
<body>
    <h1>Admin page of CabsOnline</h1>
    <p>1. Click below button to search for all unassigned booking requests with a pick-up time within 2 hours.</p>
    <form method="post">
        <input type="hidden" name="unassigned-list" value="" />
        <input type="submit" value="List all" />
    </form>
    <?php echo $unassigned; ?>
    <hr />
    <p>2. Input a reference number below and click "update" button to assign a taxi to that request.</p>
    <p><?php echo $msg; ?></p>
    <form method="POST" action="admin.php">
        <p><label>Reference number: <input type="text" name="reference_number" /></label></p>
        <p><input type="submit" value="Update" /></p>
    </form>
</body>
</html>