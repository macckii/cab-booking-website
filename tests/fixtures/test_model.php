<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test model (only for testing usage)
*/

require_once(dirname(__DIR__)."../../active_record/model.php");
require_once(dirname(__DIR__)."../../validation/custom_validations.php");

class TestModel extends Model {
    protected $fields = [
        "name",
        "phone",
        "email",
        "date",
        "time",
    ];
    
    function getValidations() {
        return [
            'name' => [
                "required",
                ['type' => "max", 'value' => 10],
            ],
            'phone' => [
                "required",
                ['type' => "equal", 'value' => 8],
            ],
            'email' => [
                "unique",
                ['type' => "max", 'value' => 30],
            ], 
            'datetime' => [
                [
                    'type'       => "custom", 
                    'fields'     => ["date", "time"],
                    'condition'  => function($v) { return strlen($v[0]) > 0 && strlen($v[1]) > 0 ? checkTimeBuffer($v[0], $v[1]) : true; },
                    'error_msg'  => "Date and time error msg",
                ],
            ],
        ];
    }

}

?>