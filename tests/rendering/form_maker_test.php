<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for form_maker.php
*/

require_once(dirname(__DIR__)."../../rendering/form_maker.php");

class FormMakerTest extends PHPUnit_Framework_TestCase {

    public function testRender() {
        $data = ['name' => "Linda", 'phone' => "0344"];
        $errors = ['phone' => "Phone must be 10 digits"];
        $formMaker = new FormMaker($data, $errors, "test", "POST", "test.php", "test");
        $formMaker->textInput("name", "Full name");
        $formMaker->html("<fieldset><legend>Contact info</legend>");
        $formMaker->textInput("phone");
        $formMaker->html("</fieldset>");
        $formMaker->submitButton("Submit");

        $expResult = "<form method=\"POST\" action=\"test.php\" name=\"test\">";
        $expResult .= "<p><label>Full name: <input type=\"text\" name=\"test[name]\" value=\"Linda\" /></label></p>";
        $expResult .= "<fieldset><legend>Contact info</legend>";
        $expResult .= "<p><label>Phone must be 10 digits: <input type=\"text\" name=\"test[phone]\" value=\"0344\" /></label></p>";
        $expResult .= "</fieldset>";
        $expResult .= "<input type=\"submit\" value=\"Submit\" />";
        $expResult .= "</form>";
        $this->assertEquals($expResult, $formMaker->render());
    }
    
}

?>