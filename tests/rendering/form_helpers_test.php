<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for form_helpers.php
*/

require_once(dirname(__DIR__)."../../rendering/form_helpers.php");
require_once(dirname(__DIR__)."/fixtures/test_model.php");

class FormHelpersTest extends PHPUnit_Framework_TestCase {

    public function testTextInput() {
        
        $attribute = "name";
        $value = "Saki";
        $error = "";
        
        $expResult = "<p><label>Name: <input type=\"text\" name=\"name\" value=\"Saki\" /></label></p>";
        
        $this->assertEquals($expResult, textInput($attribute, $value, $error));
        
    }
    
    public function testModelForm() {
        $model = new TestModel();
        
        $data = ['name' => "Tamako"];
        
        $model->setData($data);
        
        $expResult = "<form method=\"POST\" action=\"test.php\" name=\"test\">";
        $expResult .= "<p><label>Name: <input type=\"text\" name=\"testmodel[name]\" value=\"Tamako\" /></label></p>";
        $expResult .= "</form>";
        
        $this->assertEquals($expResult, modelForm($model, "POST", "test.php", "test", function($f) {
            $f->textInput("name");
        }));
        
    }
}

?>