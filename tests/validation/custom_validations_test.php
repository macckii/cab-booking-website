<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for custom_validations.php
*/

require_once(dirname(__DIR__)."../../validation/custom_validations.php");

class ValidationTest extends PHPUnit_Framework_TestCase {

    public function testCheckTimeBuffer() {
        $nowInvalid = date("d-m-Y H:i");
        $timePartsInvalid = explode(" ", $nowInvalid);
        $this->assertEquals(false, checkTimeBuffer($timePartsInvalid[0], $timePartsInvalid[1]));
        
        $nowValid = date("d-m-Y H:i", strtotime("+1 hour"));
        $timePartsValid = explode(" ", $nowValid);
        $this->assertEquals(true, checkTimeBuffer($timePartsValid[0], $timePartsValid[1]));
    }

}

?>