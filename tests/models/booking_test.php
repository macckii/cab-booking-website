<?php
/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for booking.php
*/

require_once(dirname(__DIR__)."../../models/booking.php");

class BookingTest extends PHPUnit_Framework_TestCase {

    public function testValidate() {
        
        // happy path
        $input = [
            'passenger_name' => "Maki Sugita",
            'passenger_phone' => "1234567890",
            'unit_number' => "44",
            'street_number' => "500",
            'street_name' => "Green St.",
            'suburb' => "Hawthorn",
            'destination_suburb' => "Melton",
            'pickup_date' => "2016-05-04",
            'pickup_time' => "11:20",
        ];
        $validBooking = new Booking($input);
        $this->assertEquals(true, $validBooking->validate());
        
        // invalid input
        $input['passenger_phone'] = "123";
        $invalidBooking = new Booking($input);
        $this->assertEquals(false, $invalidBooking->validate());
        
        // incomplete input
        $incompleteInput = [
            'passenger_name' => "Maki Sugita",
            'unit_number' => "44",
        ];
        $incompleteBooking = new Booking($incompleteInput);
        $this->assertEquals(false, $incompleteBooking->validate());
    }

    public function testCreate() {
        
        // There should be a customer who has the same email with the booking to be inserted
        
        // happy path, but it will fail in the test environment
        $input = [
            'passenger_name' => "Maki Sugita",
            'passenger_phone' => "1234567890",
            'unit_number' => "44",
            'street_number' => "500",
            'street_name' => "Green St.",
            'suburb' => "Hawthorn",
            'destination_suburb' => "Melton",
            'pickup_date' => "2016-04-04",
            'pickup_time' => "11:20",
            'email' => "test@test.com",
            'status' => "",
            'booking_datetime' => date("Y-m-d H:i"),
        ];

        $validBooking = new Booking($input);
        $this->assertEquals(false, $validBooking->create());

    }
}

?>