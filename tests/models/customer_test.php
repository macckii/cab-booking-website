<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for customer.php
*/

require_once(dirname(__DIR__)."../../models/customer.php");

class CustomerTest extends PHPUnit_Framework_TestCase {

    public function testValidate() {
        
        // happy path
        $input = [
            'name' => "Maki Sugita",
            'password' => "maki111",
            'confirm_password' => "maki111",
            'email' => "111@test.com",
            'phone' => "1234567890",
        ];
        $validCustomer = new Customer($input);
        $this->assertEquals(true, $validCustomer->validate());
        
        // invalid input
        $input['confirm_password'] = "123";
        $invalidCustomer = new Customer($input);
        $this->assertEquals(false, $invalidCustomer->validate());
        
        // incomplete input
        $incompleteInput = [
            'name' => "Maki Sugita",
            'password' => "maki111",
        ];
        $incompleteCustomer = new Customer($incompleteInput);
        $this->assertEquals(false, $incompleteCustomer->validate());
    }

    public function testCreate() {
        
        // happy path, but it will fail in the test environment
        $input = [
            'name' => "Maki Sugita",
            'password' => "maki111",
            'confirm_password' => "maki111",
            'email' => "111@test.com",
            'phone' => "1234567890",
        ];

        $validCustomer = new Customer($input);
        $this->assertEquals(false, $validCustomer->create());

    }
}

?>