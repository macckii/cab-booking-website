<?php

/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for functional.php
*/

require_once(dirname(__DIR__)."../../utils/functional.php");

class FunctionalUtilsTest extends PHPUnit_Framework_TestCase {

    public function testUnpackFlatten() {
        $testInput = ['finn' => "human", 'jake' => "dog"];
        $testResult = unpackFlatten($testInput, ["finn", "jake"]);
        $this->assertEquals(["human", "dog"], $testResult);
        
        $testInput = ['bubblegum' => "", 'jake' => "dog"];
        $testResult = unpackFlatten($testInput, ["bubblegum", "jake"]);
        $this->assertEquals(["", "dog"], $testResult);
        
        $testInput = ['bubblegum' => "", 'jake' => "dog"];
        $testResult = unpackFlatten($testInput, ["finn", "jake"]);
        $this->assertEquals(["dog"], $testResult);
    }
}

?>