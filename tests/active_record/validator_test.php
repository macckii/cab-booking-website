<?php
/*
    author: Maki Sugita
    student ID: 100059374
    description: Test for validator.php
*/

require_once(dirname(__DIR__)."../../active_record/validator.php");
require_once(dirname(__DIR__)."/fixtures/test_model.php");

class ValidatorTest extends PHPUnit_Framework_TestCase {

    public function testValidate() {
        
        $now = date("d-m-Y H:i");
        $timeParts = explode(" ", $now);
        
        $invalidInputMock1 = ['name' => "xxxxxxxxxxxxxx", 'date' => $timeParts[0], 'time' => $timeParts[0]];
        $invalidInputMock2 = ['phone' => "88888888", 'name' => "", 'email' => "test@test.com"];
        $invalidInputMock3 = ['phone' => "", 'name' => "", 'email' => "111@test.com"];
        $validInputMock    = ['name' => 'xxxxxxx', 'address' => "xxx", 'phone' => "03999999"];
        
        $testObj = new TestModel();
        
        $validator = new Validator($testObj);
        
        $testObj->setData($invalidInputMock1);
        $this->assertEquals(['name' => "Name must be less than 10 characters/digits", 'phone' => "Phone is required", 'datetime' => "Date and time error msg"], $validator->validate());
        
        $testObj->setData($invalidInputMock2);
        $this->assertEquals(['name' => "Name is required"], $validator->validate());
        
        $testObj->setData($invalidInputMock3);
        $this->assertEquals(['name' => "Name is required", 'phone' => "Phone is required"], $validator->validate());
        
        $testObj->setData($validInputMock);
        $this->assertEquals([], $validator->validate());
    }

}

?>